# Mutation Challenge

_Autor: Leonardo Abundis - <leonardo.abundis.mos@gmail.com>_

Programa que detecta si una persona tiene diferencias genéticas basándose en
su secuencia de ADN. 

Recibe como parámetro un arreglo de cadena caracteres que representan cada fila de una tabla de NxN
con la secuencia del ADN. Las letras de los caracteres solo pueden ser: `A, T, C, G;` las cuales representan
cada base nitrogenada del ADN.

## Ejemplo

En la imagen se muestra una secuencia sin mutación del lado izquierdo y una secuencia con diversas mutaciones del lado derecho. Existe una mutación si se encuentra más de una secuencia de cuatro letras iguales, de forma oblicua, horizontal o vertical.

![Semantic description of image](/Images/ejemplo.png "Ejemplo de mutación")

## Desafío: Nivel 1

Crear un programa con un método o función que devuelva `true` o `false` dependiendo si hay mutación o no.

`boolean hasMutation(String[] dna);`

A la función se le pasará una lista con la secuencia de ADN como se muestra a continuación.

`String[] dna = { "ATGCGA", "CAGTGC", "TTATGT", "AGAAGG", "CCCCTA","TCACTG" };`

En el ejemplo anterior, la función `hasMutation(dna)` devuelve `true` debido a que encuentra una mutación en la secuencia de ADN.

### Solución

La solución se realizó usando `javascript`, se crearon diversas funciones y se separaron en módulos. Acontinuación se mencionan cada uno de los módulos y su funcionalidad.

- `hasMutation` módulo principal, verifica si existe mutación y de que tipo. Utiliza todos los módulos
- `isValid` módulo que verifica si la secuencia contiene los carácteres válidos 
- `mutation` módulo que verifica si existe mutación
- `isHorizontal` módulo que analiza si existe mutación de manera horizontal
- `isVertical` módulo que analiza si existe mutación de manera vertical
- `isOblique` módulo que analiza si existe mutación de manera oblicua
- `isObliqueInvert` módulo que analiza si existe mutación de manera oblicua invertida

Para probar la solución se utiliza el archivo `test.js` (el archivo puede ser editado para probar diferentes secuencias) este archivo contiene 3 diferentes secuencias de ADN e imprime `true` o `false` dependiendo si se encuentra una mutación en la secuencia.

1. Editar el archivo `test.js` con las secuencias que quieras probar
2. Tener instalado **node.js** ( <https://nodejs.org/es/download/> )
3. Abrir un **cmd** y colocarte en el directorio (`C:\*tu directorio*`) donde se encuentra tu archivo `test.js`
4. Ejecutar el comando `node test.js`

## Desafío: Nivel 2

Crear una **API REST**, para hostear esta API en un cloud computing libre (AWS, Azure DevOps, GCP, etc.), crear el
endpoint `POST /mutation` en donde se pueda detectar si existe mutación enviando la secuencia de ADN
mediante un **JSON** el cual tenga el siguiente formato:

```json
POST /mutation
{
    "dna":["ATGCGA", "CAGTGC", "TTATGT", "AGAAGG", "CCCCTA", "TCACTG"]
}
```

En caso de encontrar una mutación, devolver un `200`, en caso contrario un `403`.

### Solución

Se implementó el código en el servicio serverless de **AWS** llamado **Lambda** y se siguieron los siguientes pasos.

1. Se cargaron los archivos a la función Lambda, la cual usa `node.js` 
2. Usando el archivo `index.js` se programó la función para manejar la petición `POST`
3. Se creo la **API REST** usando la herramienta de Amazon API Gateway
4. Se vinculó la API con la función **Lambda**
5. Se creo el recurso para la petición `POST`
6. Se creo el método `POST`en el recurso
7. La función **Lambda** retorna si se encuentra una mutación en la secuencia de ADN

URL de la API para realizar el `POST` request.

<https://kxqakuzdc2.execute-api.us-east-1.amazonaws.com/test1/mutation>

Formato del **JSON** para realizar el `POST` request.
```json
{
    "dna":["ATGCGA", "CAGTGC", "TTATGT", "AGAAGG", "CCCCTA", "TCACTG"]
}
```

Respuesta de la API si **se encuentra** mutación.

```json
{
  "statusCode": 200,
  "body": "\"Mutation!\""
}
```

Respuesta de la API si **no se encuentra** mutación.

```json
{
  "statusCode": 403,
  "body": "No Mutation!"
}
```

Revisar las imagenes en el directorio `Images` para más referencias.

## Desafío: Nivel 3

Anexar una base de datos, la cual guarde los ADNs verificados con la **API**. Sólo 1 registro por ADN. Exponer unservicio extra `GET /stats` que devuelva un **JSON** con las estadísticas de las verificaciones de ADN, como se muestra a continuación:

```json
{
    "count_mutations": 40,
    "count_no_mutation": 100,
    "ratio": 0.4
}
```
### Solución

Para resolver este desafío, se implementó otra herramienta de AWS llamda DynamoDB, la cual es una base de datos no relacional.

1. Se creo la tabla en la herramienta DynamoDB. 
2. Se configuró con 3 columnas para cada registro, 
    - Identificador único
    - Mutación (si o no) 
    - No Mutación (si o no)
3. Se creo el recurso para la petición `GET` en la API
4. Se creo el método `GET` en el recurso
5. Usando el archivo `index.js` se programó la función para manejar la petición `GET`
6. En la función que maneja la petición `GET` se hace la consulta a la base de datos, para tomar los datos de los registros

URL de la API para realizar el `GET` request.

<https://kxqakuzdc2.execute-api.us-east-1.amazonaws.com/test1/stats>

Respuesta de la API a la petición `GET`.

```json
{
  "count_mutations": 2,
  "count_no_mutation": 2,
  "ratio": 1
}
```

Revisar las imagenes en el directorio `Images` para más referencias.
