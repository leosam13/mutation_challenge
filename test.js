/***********************************
 *
 * Begin test
 * 
 ***********************************/
const hasMutation = require('./modules/hasMutation');

// Dna Matrix test cases, edit these lines for your personal test
const dna1 = [
    "ATGCGA",
    "CAGTGC",
    "TTGTGT",
    "AGGAGG",
    "CCACTA",
    "TCACTG"
  ];
  
  const dna2 = [
    "AATAGA",
    "CAGTGC",
    "TTGTGT",
    "AGCAGG",
    "CCACTA",
    "TCACTG"
  ];
  
  const dna3 = [
    "ATGCGA",
    "ATCGTA",
    "AGCGTA",
    "ATGCGC",
    "CCACAA",
    "CACACA"
  ];
  
  //Test implementation
  console.log(hasMutation(dna1));
  console.log(hasMutation(dna2));
  console.log(hasMutation(dna3));