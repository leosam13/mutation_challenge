// Loads in the AWS SDK
const AWS = require('aws-sdk');

// Loads the code in modules
const hasMutation = require('./modules/hasMutation');

const ddb = new AWS.DynamoDB.DocumentClient({region: 'us-east-1'});

// Set response with a code of error
let response = {
            statusCode: 111,
            body: JSON.stringify('Error!'),
};

//Function to create an entry to the dynamoDB
function isMutation(requestId) {
    const params = {
        TableName: 'mutationsStats',
        Item: {
            'ID': requestId,
            'mutation' : 1,
            'No_mutation' : 0
        }
    };
    return ddb.put(params).promise();
}

//Function to create an entry to the dynamoDB
function noMutation(requestId) {
    const params = {
        TableName: 'mutationsStats',
        Item: {
            'ID': requestId,
            'mutation' : 0,
            'No_mutation' : 1
        }
    };
    return ddb.put(params).promise();
}

//Function to read entries from dynamoDB
function readStats() {
    const params = {
        TableName: 'mutationsStats',
    }
    return ddb.scan(params).promise();
}

//Main Functions
exports.handler = async (event, context, callback) => {
    
    // Handle the GET request
    if (event.httpMethod == 'GET' ){
        
        await readStats().then(data => {
            
        let items = data.Items;
        let countMutation = 0;
        let countNoMutation = 0;
        
        Object.entries(items).forEach(item => {
            if (item[1].mutation == 1){
                countMutation = countMutation + 1; 
            }
            else{
                countNoMutation = countNoMutation +1;
            }
        })
        
        console.log(countMutation)
        
        callback(null, {
            statusCode: 200,
            body:JSON.stringify({
                    'count_mutations': countMutation,
                    'count_no_mutation': countNoMutation,
                    'ratio': countMutation/countNoMutation
                 }) 
        })
        }).catch((err) => {
            // If an error occurs write to the console
            console.error(err);
        });
    }
    
    // Handle the POST request
    else {
        let dna = event.dna;
        
        if (hasMutation(dna)) {
            const requestId = context.awsRequestId;
            
            await isMutation(requestId).then(() => {
            callback(null, {
                statusCode: 200,
                body: JSON.stringify('Mutation!'),
            });
            }).catch((err) => {
                console.error(err);
            });
        }
        
        else {
            const requestId = context.awsRequestId;
            
            await noMutation(requestId).then(() => {
            callback(null, {
                statusCode: 403,
                body: 'No Mutation!',
            });
            }).catch((err) => {
                console.error(err);
            });
        }
        
        return response;
    }
};
