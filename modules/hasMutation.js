const isHorizontal = require('./isHorizontal');
const isVertical = require('./isVertical');
const isOblique = require('./isOblique');
const isObliqueInvert = require('./isObliqueInvert');
const isValid = require('./isValid');
const Mutation = require('./mutation');

function hasMutation(dna) {
  const rows = dna.length
  const cols = dna[0].length
  let mutationsCount = 0

  if ((rows >= 4) || (cols >= 4)) {
    dna.forEach((element, row) => {
      for(let col = 0; col < element.length; col++) {
        if (isValid(dna[row].charAt(col))) {
          if (!Mutation.existMutation([col, row])) {
            if (isHorizontal(col, row, dna) || isVertical(col, row, dna) || isOblique(col, row, dna) || isObliqueInvert(col, row, dna)) {
              mutationsCount++
            }
          }
        } else {
          throw new Error('Caracter inválido en la secuencia, verificar que la secuencia tenga solo los siguientes carácteres: A, T, C, G')
        }
      }
    });
  }

  Mutation.resetData()

  if (mutationsCount >= 2) {
    return true
  }

  return false
}

module.exports = hasMutation